# **************************************************************************************************
# N�zov: PGN_Analyzer.rb
#
# Verzia: 0.1 (prva verejna verzia)
#
# Stav: rozpracovany/under development
#
# Popis: program robi analyzu zapisu sachovej partie vo formate PGN 
#
# Vstup: nic na prikazovom riadku, vstupom od uzivatela je nazov 
#        textoveho suboru, v ktorom je zapis partie ulozeny
#
# Vystup: program vypise na vystup retazec "ok", ak je zapis sachovej partie
#         v poriadku, ak nie je vypise na vystup retazec "err"
#
# Struktura volania metod:
#
#       <main>
#       --- Analyzuj
#       ------ AnalyzujHlavicku
#       --------- AnalyzujRiadokHlavicky
#       ------ AnalyzujTahy
#       --------- is_result_token       
#       --------- analyzuj_tah       
#       ------------ dvojzntah
#       ------------ trojzntah
#       ------------ stvorzntah
#       ------------ patzntah
#
# Poznamky: nateraz (v.0.1) je urobena len lexikalna analyza, teda kontroluje sa
#           ci je sachova partia formalne spravne zapisana vo formate PGN, nekontroluje sa
#           ci zapisane tahy su korektne z hladiska sachovych pravidiel, ani to, ake tagy
#           su ci nie su uvedene v hlavicke programu. Je implementovana zakladna funkcionalita 
#           co sa tyka PGN formatu, nie je to plna implementacia PGN standardu.
#           Nie je urobena ani chybova diagnostika, v pripade chyby, program jednoducho 
#           vypise "err" a skonci neindikujuc co, preco a na ktorom mieste je zle.
#           Nie su vytvorene ani testovacie procedury/skripty a dokumentacia.           
#
# Hist�ria zmien:
#	   22.06.2007 - v.0.1 - Slavo FURMAN - prva verejna verzia 
# **************************************************************************************************

# ==================================================================================================
# metoda: Analyzuj.rb
#
# Popis: metoda robi analyzu zapisu sachovej partie vo formate PGN 
#
# Vstup: retazec, v ktorom je cely zapis sachovej partie, ako sme ho precitali zo suboru
#
# Vystup: vrati retazec "ok" alebo "err", podla toho ako dopadla analyza 
# ==================================================================================================

def analyzuj(pgnsubor)

  result = "ok"

  hlavicka = ""
  tahy = ""

  citamehlavicku = 0
  citametahy = 0

  # postupne citame obsah vstupneho suboru (ktory sme dostali uz ako retazec)
  # riadok po riadku a do osobitnych retazcov ulozime cast s hlavicku partie 
  # a cast s tahmi partie

  pgnsubor.each_line do |riadok|

    if (riadok.strip.length !=0)

      if (citamehlavicku == 0)
          citamehlavicku = 1
          hlavicka = hlavicka + riadok
      
      elsif (citamehlavicku == 1)
      
         hlavicka = hlavicka + riadok
      
      elsif ((citamehlavicku==2) and (citametahy==0))
      
        citametahy=1
        tahy = tahy + riadok
      
      elsif ((citamehlavicku==2) and (citametahy==1))
      
        tahy = tahy + riadok
      
      end
    
    else
    
      if (citamehlavicku == 1)   
        citamehlavicku = 2    
      elsif (citametahy == 1)    
        citametahy = 2
      end
    
    end
  
  end  
  
  #puts "HLAVICKA:"
  #puts hlavicka
 
  #puts "TAHY:"
  #puts tahy
  
  
  #cast s hlavickou analyzujeme v metode "analyzujhlavicku"
  result = analyzujhlavicku(hlavicka)
  if (result !="ok")
      return "err"
  end
  
  #cast s tahmi analyzujeme v metode "analyzujtahy" - volane len ak hlavicka je "ok"
  if (result == "ok")
    result = analyzujtahy(tahy)
  end

  return result
  
end

# ==================================================================================================
# metoda: analyzujhlavicku.rb
#
# Popis: metoda robi analyzu hlavicky zapisu sachovej partie vo formate PGN 
#
# Vstup: retazec, v ktorom je hlavicka zapisu sachovej partie
#
# Vystup: vrati retazec "ok" alebo "err", podla toho ako dopadla analyza 
# ==================================================================================================

def analyzujhlavicku(hlavicka)
  
  result = "ok"
  
  # berieme postupne vsetky riadky hlavicky a analyzujeme ich v metode "analyzujriadokhlavicky"
  hlavicka.each_line do |riadok|
    result = analyzujriadokhlavicky(riadok.chomp)
      if (result == "err")
        return result
      end
  end
  
  return result
end

# ==================================================================================================
# metoda: analyzujhlavicku.rb
#
# Popis: metoda robi analyzu riadku hlavicky zapisu sachovej partie vo formate PGN 
#
# Vstup: retazec, v ktorom je riadok z hlavicky zapisu sachovej partie vo formate PGN 
#
# Vystup: vrati retazec "ok" alebo "err", podla toho ako dopadla analyza 
# ==================================================================================================

def analyzujriadokhlavicky(riadok)

pos = 0
dlzkar = riadok.length

#puts "analyzujeme -> {" + riadok + "} "
#puts "dlzkar -> {" + dlzkar.to_s + "} "

result = "ok"

#puts "riadok[0] -> {" + riadok[0].to_s + "} "
#puts "riadok[dlzkar-1] -> {" + riadok[dlzkar-1].to_s + "} "

# riadok musi zacinat a koncit hranatou zatvorkou
if (riadok[0].chr.to_s != "[") or (riadok[dlzkar-1].chr.to_s != "]")
  #puts "arh-CHYBA1!"
  return "err"
end

# preskocime pripadne biele znaky za otvaracou hranatou zatvorkou
# (pri vsetkych citaniach testujeme aj to ci sa nedostaneme az na koniec
# analyzivaneho retazca, co by bola chyba)
pos = 1
while ((riadok[pos].chr.to_s ==" ") or (riadok[pos].chr.to_s == "\t"))
  pos = pos + 1
   if (pos > dlzkar-2)
     #puts "arh-CHYBA2!"
     return "err"
   end
end

#puts "pos -> {" + pos.to_s + "} "

# precitame tagname do osobitnej premennej, moze sa hodit, ak v buducnosti budeme 
# testovat aj hodnotu/obsah/vyznam tagname
tagname = ""
while ((riadok[pos].chr.to_s !=" ") and (riadok[pos].chr.to_s != "\t"))

  #puts "riadok[" + pos.to_s + "] -> " + riadok[pos].to_s + "(" + riadok[pos].chr + ")"
  #puts "pos -> {" + pos.to_s + "}, tagname -> {" + tagname + "}"
  
  tagname = tagname + riadok[pos].chr.to_s
  pos = pos + 1
   if (pos > dlzkar-2)
     #puts "arh-CHYBA3!"
     return "err"
   end
end

#puts "tagname -> {" + tagname + "} "

# preskocime biele znaky za tagname
while ((riadok[pos].chr.to_s ==" ") or (riadok[pos].chr.to_s == "\t"))
  pos = pos + 1
   if (pos > dlzkar-2)
     #puts "arh-CHYBA4!"
     return "err"
   end
end

# hodnota tagname musi zacinat uvodzovkami
if (riadok[pos].chr.to_s !="\"")
     #puts "arh-CHYBA5!"
     return "err"
else     
     pos = pos + 1     
end     

# precitame hodnotu tagvalue do osobitnej premennej
tagvalue = ""
while (riadok[pos].chr.to_s != "\"")
  
  tagvalue = tagvalue + riadok[pos].chr.to_s
  pos = pos + 1
  
  if (pos > dlzkar-2)
     #puts "arh-CHYBA6!"
     return "err"
  end
end

#puts "tagvalue -> {" + tagvalue + "} "

# preskocime pripadne biele znaky pre zatvaracou hranatou zatvorkou
while (pos > dlzkar-2)

  if ((riadok[pos].chr.to_s !=" ") or (riadok[pos].chr.to_s != "\t"))
     #puts "arh-CHYBA7!"
     return "err"
   end
end

#ak sme sa bez chyb dostali az tu, je analyzovany retazec ok
return "ok"
end

# ==================================================================================================
# metoda: analyzujtahy.rb
#
# Popis: metoda robi analyzu tahov zapisu sachovej partie vo formate PGN 
#
# Vstup: retazec, v ktorom su tahy zo zapisu sachovej partie vo formate PGN 
#
# Vystup: vrati retazec "ok" alebo "err", podla toho ako dopadla analyza 
# ==================================================================================================

def analyzujtahy(tahy)
 
  pos = 0
  dlzkat = tahy.length
  
  result = "ok"
  
  #puts "TAHY (" + dlzkat.to_s + "):"
  #puts " " 
  #puts tahy 
  #puts " "
  
  cislo_aktualne_sprac_tahu = 0
  nasledujuci_token = "nct"

  # prechadzame cely vstupny retazec a berieme jeden token za druhym 
  # a posielame ich do prislusnych metod na analyzu. Tokeny su od seba 
  # oddelene bielymi znakmi.
  #
  # Druhy tokenov su:
  #                    - cislo tahu
  #                    - tah bieleho
  #                    - tah cierneho
  #                    - vysledok
  
  while (pos < dlzkat)

    # preskocime biele znaky
    while ((pos > dlzkat-1) or (tahy[pos].chr.to_s ==" ") or (tahy[pos].chr.to_s == "\t") or (tahy[pos].chr.to_s == "\n"))
       pos = pos + 1
    end 
    
    # precitame nasledujuci token
    
    next_token = ""
    
    while ((pos < dlzkat) and (tahy[pos].chr.to_s !=" ") and (tahy[pos].chr.to_s != "\t") and (tahy[pos].chr.to_s != "\n"))

       #puts "tahy[" + pos.to_s + "] -> " + tahy[pos].to_s + "(" + tahy[pos].chr + ")"
       #puts "pos -> {" + pos.to_s + "}, next_token -> {" + next_token + "}" 
    
       next_token = next_token + tahy[pos].chr.to_s
       pos = pos + 1

    end
    
    #puts "next_token -> {" + next_token + "}"    
    
    
    #zistime ci dany token predstavuje zapis vysledku sachovej partie
    is_result = is_result_token(next_token)
    
    # ak ano, dalsi token uz nema nasledovat
    if (is_result == "result")
       
        nasledujuci_token == "nothing_more"    

    else    

        # ak bol predtym zapis vysledku, tak je to chyba, lebo za vysledok uz nema byt nic 
        if (nasledujuci_token == "nothing_more")
        
             puts "at-CHYBA1!"
             return "err"

        # ak ma nasledovat token s cislo tahu, testujeme ci je to naozaj cislo tahu
        elsif (nasledujuci_token == "nct")
        
           ntoken = (cislo_aktualne_sprac_tahu + 1).to_s + "."
           
           #puts "ntoken -> {" + ntoken + "}"
           
           if (next_token != ntoken)
             puts "at-CHYBA2!"
             return "err"
           else
             cislo_aktualne_sprac_tahu = cislo_aktualne_sprac_tahu + 1
             nasledujuci_token = "tb"
           end     
           
        # ak ma nasledovat token "tah bieleho" alebo "tah cierneho", tak zmenime typ 
        # nasledujuceho tokenu a testujeme ci je dany token koretne zapisany tah
        elsif (nasledujuci_token == "tb") 
        
             nasledujuci_token = "tc"
             
             if (analyzuj_tah(next_token) == "err")
               puts "at-CHYBA3!"
               return "err"             
             end 


        elsif (nasledujuci_token == "tc") 

             nasledujuci_token = "nct"
             
             if (analyzuj_tah(next_token) == "err")
               puts "at-CHYBA4!"
               return "err"             
             end 
           
        end      
         
     end    

  end # koniec vonkajsieho cyklu na spracovanie tahov
  
  # ak posledny token nebol vysledok je to chyba
  if (is_result != "result")
  
     result = "err"
  
  end   
    
  return result
  
end

# ==================================================================================================
# metoda: is_result_token.rb
#
# Popis: metoda robi analyzu tokenu (retazca, ktory obsahuje bud cislo tahu, tah, alebo zapis 
#        vysledku sachovej partie). Konkretne zistuje ci dany token predstavuje zapis vysledku.
#        Zapis vysledku musi byt v formate:
#                                           "1-0"
#                                           "0-1"
#                                           "1/2-1/2"
#
# Vstup: retazec, v ktorom analyzovany token
#
# Vystup: vrati retazec "result", ak token predstavuje zapis vysledku, inak vrati retazec "noresult"
# ==================================================================================================

def is_result_token(token)
  
  irt = "noresult"
  
  if (token == "1-0")
     irt = "result"
  elsif (token == "0-1")
     irt = "result"
  elsif (token == "1/2-1/2")
     irt = "result"
  else
     irt = "noresult"    
  end 
     
  return irt
  
end

# ==================================================================================================
# metoda: analyzuj_tah.rb
#
# Popis: metoda robi analyzu tokenu predstavujuceho jeden tah v zapise sachovej 
#        partie vo formate PGN. Nateraz sa robi len lexikalna analyza, teda zistuje 
#        sa ci je tah formalne spravne zapisany, nekontroluje sa, ci je dany tah mozny/spravny
#        vhladom na pravidla sachu/aktualne postavenie figur na sachovnici. Je implementovana 
#        zakladna funkcionalita co sa tyka PGN formatu, nie je to plna implementacia PGN standardu.
#        nerozlisuje ci ide o tah bieleho alebo cierneho, co by sme do buducnosti mohli - bud v jednej
#        alebo dvoch metodach. Umoznilo by to presnejsiu analyzu tahu, najma ak by islo aj o kontrolu
#        jeho spravnosti podla sachovych pravidiel.
#
# Vstup: retazec, v ktorom analyzovany token
#
# Vystup: vrati retazec "ok" alebo "err", podla toho ako dopadla analyza 
# ==================================================================================================

def analyzuj_tah(tah)
  
  dt = tah.length
  up_tah = ""
  
  # ak je za tahom znak oznacujuci sach alebo mat, tak ho z tokenu odstranime
  # kedze v pri terajsej analyze nema pre nas vyznam. Upraveny token/tah ulozime
  # do novej premennej "up_tah"
  if ((tah[dt-1].chr == "+") or (tah[dt-1].chr == "#"))
  
     i = 0
     
     while (i < dt-1)     
        up_tah = up_tah + tah[i].chr.to_s
        i = i + 1
     end
  
  else  
     up_tah = tah
  end
  
  #puts up_tah
  
  # ak ide o malu alebo velku rosadu, tak je vsetko "ok" a koncime
  if ((up_tah == "O-O") or (up_tah == "O-O"))
  
    result = "ok"
    return result 
  
  end
    
  # zistime dlzku tahu, a volame prislusnu metodu na jeho analyzu. 
  # Dlhsie tahy ako 5-znakove (podla nasej analyzy) nemozu existovat.
    
  if (up_tah.length == 2)
      
     result = dvojzntah(up_tah)
     return result 
  
  elsif (up_tah.length == 3)
  
    result = trojzntah(up_tah)
    return result 

  elsif (up_tah.length == 4)
  
    result = stvorzntah(up_tah)
    return result 

  elsif (up_tah.length == 5)
  
    result = patzntah(up_tah)
    return result 

  else
  
    puts "problem s tahom - [" + up_tah + "]"
    puts "nespravny pocet znakov v tahu"
    result = "err"
    return result 
     
  end
  
  result = "ok"
  return result
  
end

# ==================================================================================================
# metoda: dvojzntah.rb
#
# Popis: metoda robi analyzu tokenu predstavujuceho dvojznakovy tah v zapise sachovej 
#        partie vo formate PGN. Nateraz sa robi len lexikalna analyza, teda zistuje 
#        sa ci je tah formalne spravne zapisany, nekontroluje sa, ci je dany tah mozny/spravny
#        vhladom na pravidla sachu/aktualne postavenie figur na sachovnici. Je implementovana 
#        zakladna funkcionalita co sa tyka PGN formatu, nie je to plna implementacia PGN standardu.
#
#        Pri dvojznakovom tokene ide o tah pesiakom (kde nie je branie ani premena pesiaka), 
#        kde prvy znak musi byt "a-h" a druhy znak (2-7)
#
# Vstup: retazec, v ktorom analyzovany token
#
# Vystup: vrati retazec "ok" alebo "err", podla toho ako dopadla analyza 
# ==================================================================================================


def dvojzntah(akt_tah)

  result = "ok"

  #puts "dvojznakovy tah (2) - [" + akt_tah + "]"

  if ((akt_tah[0].chr.to_s < "a") or (akt_tah[0].chr.to_s > "h"))
     
        result = "err"
     
  end  
     
  if ((akt_tah[1].chr.to_s < "2") or (akt_tah[1].chr.to_s > "7"))
     
        result = "err"
     
  end 

  return result

end

# ==================================================================================================
# metoda: trojzntah.rb
#
# Popis: metoda robi analyzu tokenu predstavujuceho trojznakovy tah v zapise sachovej 
#        partie vo formate PGN. Nateraz sa robi len lexikalna analyza, teda zistuje 
#        sa ci je tah formalne spravne zapisany, nekontroluje sa, ci je dany tah mozny/spravny
#        vhladom na pravidla sachu/aktualne postavenie figur na sachovnici. Je implementovana 
#        zakladna funkcionalita co sa tyka PGN formatu, nie je to plna implementacia PGN standardu.
#
#        Pri trojznakovom tokene ide o tah pesiakom, ked sa premiena na inu figuru dosiahnuc 
#        svoj posledny rad. Alebo moze ist o bezny tah figurou bez brania.
#
# Vstup: retazec, v ktorom analyzovany token
#
# Vystup: vrati retazec "ok" alebo "err", podla toho ako dopadla analyza 
# ==================================================================================================

def trojzntah(akt_tah)

   if ((akt_tah[0].chr.to_s >= "a") and (akt_tah[0].chr.to_s <= "h"))
     
       if ((akt_tah[1].chr.to_s != "1") and (akt_tah[1].chr.to_s != "8"))
       
          #puts "problem s trojznakovym tahom (1) - [" + akt_tah + "]"
          result = "err"
          return result      
       
       else 
       
         if ((akt_tah[2].chr.to_s != "N") and (akt_tah[2].chr.to_s != "B") and
             (akt_tah[2].chr.to_s != "R") and (akt_tah[2].chr.to_s != "Q"))
         
            #puts "problem s trojznakovym tahom (2) - [" + akt_tah + "]"
            result = "err"
            return result      
         
         else
         
            #puts "trojznakovy tah (3) - [" + akt_tah + "]"
            result = "ok"
            return result                
       
         end   

       end
       
   elsif ((akt_tah[0].chr.to_s == "N") or (akt_tah[0].chr.to_s == "B") or
           (akt_tah[0].chr.to_s == "R") or (akt_tah[0].chr.to_s == "Q") or (akt_tah[0].chr.to_s == "K"))
       
       if ((akt_tah[1].chr.to_s < "a") or (akt_tah[1].chr.to_s > "h"))
       
          #puts "problem s trojznakovym tahom (4) - [" + akt_tah + "]"
          result = "err"
          return result      
       
       else  
       
         if ((akt_tah[2].chr.to_s < "1") or (akt_tah[2].chr.to_s > "8"))
         
            #puts "problem s trojznakovym tahom (5) - [" + akt_tah + "]"
            result = "err"
            return result   
            
         else
         
            #puts "trojznakovy tah (6) - [" + akt_tah + "]"
            result = "ok"
            return result                
       
         end   

       end
       
   else
     
       #puts "problem s trojznakovym tahom (7) - [" + akt_tah + "]"
       result = "err"
       return result  

   end 

   #puts "problem s trojznakovym tahom (8) - [" + akt_tah + "]" 
   result = "err"
   return result

end

# ==================================================================================================
# metoda: stvorzntah.rb
#
# Popis: metoda robi analyzu tokenu predstavujuceho stvorznakovy tah v zapise sachovej 
#        partie vo formate PGN. Nateraz sa robi len lexikalna analyza, teda zistuje 
#        sa ci je tah formalne spravne zapisany, nekontroluje sa, ci je dany tah mozny/spravny
#        vhladom na pravidla sachu/aktualne postavenie figur na sachovnici. Je implementovana 
#        zakladna funkcionalita co sa tyka PGN formatu, nie je to plna implementacia PGN standardu.
#
#        Pri stvorznakovom tokene ide o branie pesiakom, alebo branie figurou, alebo tah
#        figurou, ked na dane miesto mozu ist dve figury naraz.
#
# Vstup: retazec, v ktorom analyzovany token
#
# Vystup: vrati retazec "ok" alebo "err", podla toho ako dopadla analyza 
# ==================================================================================================

def stvorzntah(akt_tah)

   if ((akt_tah[0].chr.to_s >= "a") and (akt_tah[0].chr.to_s <= "h"))
     
       if (akt_tah[1].chr.to_s != "x") 
       
          #puts "problem so stvorznakovym tahom (1) - [" + akt_tah + "]"
          result = "err"
          return result      
       
       else 
       
         if ((akt_tah[2].chr.to_s < "a") or (akt_tah[2].chr.to_s > "h"))
         
            #puts "problem so stvorznakovym tahom (2) - [" + akt_tah + "]"
            result = "err"
            return result      
         
         else
         
            if ((akt_tah[3].chr.to_s < "2") and (akt_tah[3].chr.to_s > "7"))
         
                #puts "problem so stvorznakovym tahom (3) - [" + akt_tah + "]"
                result = "err"
                return result  
            
            else
                        
                #puts "stvorznakovy tah (4) - [" + akt_tah + "]"
                result = "ok"
                return result 
            
            end               
       
         end   

       end
       
   elsif ((akt_tah[0].chr.to_s == "N") or (akt_tah[0].chr.to_s == "B") or
           (akt_tah[0].chr.to_s == "R") or (akt_tah[0].chr.to_s == "Q") or (akt_tah[0].chr.to_s == "K"))
                      
       if (akt_tah[1].chr.to_s == "x") 
       
           if ((akt_tah[2].chr.to_s < "a") or (akt_tah[2].chr.to_s > "h"))
           
              #puts "problem so stvorznakovym tahom (5) - [" + akt_tah + "]"
              result = "err"
              return result      
           
           else
                     
              if ((akt_tah[3].chr.to_s < "1") or (akt_tah[3].chr.to_s > "8"))
           
                  #puts "problem so stvorznakovym tahom (6) - [" + akt_tah + "]"
                  result = "err"
                  return result  
              
              else
                          
                  #puts "stvorznakovy tah (7) - [" + akt_tah + "]"
                  result = "ok"
                  return result 
              
              end               
         
           end   
       
       else
       
           if ((akt_tah[1].chr.to_s >= "1") and (akt_tah[1].chr.to_s <= "8"))
           
               if ((akt_tah[2].chr.to_s < "a") or (akt_tah[2].chr.to_s > "h"))
               
                  #puts "problem so stvorznakovym tahom (8) - [" + akt_tah + "]"
                  result = "err"
                  return result      
               
               else
                         
                  if ((akt_tah[3].chr.to_s < "1") or (akt_tah[3].chr.to_s > "8"))
               
                      #puts "problem so stvorznakovym tahom (9) - [" + akt_tah + "]"
                      result = "err"
                      return result  
                  
                  else
                              
                      #puts "stvorznakovy tah (10) - [" + akt_tah + "]"
                      result = "ok"
                      return result 
                  
                  end               
             
               end   
           
           elsif ((akt_tah[1].chr.to_s >= "a") and (akt_tah[1].chr.to_s <= "h"))
           
               if ((akt_tah[2].chr.to_s < "a") or (akt_tah[2].chr.to_s > "h"))
               
                  #puts "problem so stvorznakovym tahom (11) - [" + akt_tah + "]"
                  result = "err"
                  return result      
               
               else
                         
                  if ((akt_tah[3].chr.to_s < "1") or (akt_tah[3].chr.to_s > "8"))
               
                      #puts "problem so stvorznakovym tahom (12) - [" + akt_tah + "]"
                      result = "err"
                      return result  
                  
                  else
                              
                      #puts "stvorznakovy tah (13) - [" + akt_tah + "]"
                      result = "ok"
                      return result 
                  
                  end               
             
               end   
           
           else
           
                  #puts "problem so stvorznakovym tahom (14) - [" + akt_tah + "]"
                  result = "err"
                  return result
       
           end
       
       end
       
   else    

  
       #puts "problem s stvorznakovym tahom (15) - [" + akt_tah + "]" 
       result = "err"
       return result
  
   end

   #puts "problem s stvorznakovym tahom (16) - [" + akt_tah + "]" 
   result = "err"
   return result

end

# ==================================================================================================
# metoda: patzntah.rb
#
# Popis: metoda robi analyzu tokenu predstavujuceho patznakovy tah v zapise sachovej 
#        partie vo formate PGN. Nateraz sa robi len lexikalna analyza, teda zistuje 
#        sa ci je tah formalne spravne zapisany, nekontroluje sa, ci je dany tah mozny/spravny
#        vhladom na pravidla sachu/aktualne postavenie figur na sachovnici. Je implementovana 
#        zakladna funkcionalita co sa tyka PGN formatu, nie je to plna implementacia PGN standardu.
#
#        Pri patznakovom tokene ide o tah figurou, ked na dane miesto mozu ist dve figury naraz,
#        a sucasne je to branie.
#
# Vstup: retazec, v ktorom analyzovany token
#
# Vystup: vrati retazec "ok" alebo "err", podla toho ako dopadla analyza 
# ==================================================================================================

def patzntah(akt_tah)

   if ((akt_tah[0].chr.to_s >= "a") and (akt_tah[0].chr.to_s <= "h"))  
     
       if (akt_tah[1].chr.to_s != "x") 
       
          #puts "problem s patznakovym tahom (1) - [" + akt_tah + "]"
          result = "err"
          return result      
       
       else 
       
         if ((akt_tah[2].chr.to_s < "a") or (akt_tah[2].chr.to_s > "h")) 
         
            #puts "problem s patznakovym tahom (2) - [" + akt_tah + "]"
            result = "err"
            return result      
         
         else 
         
            if ((akt_tah[3].chr.to_s != "1") and (akt_tah[3].chr.to_s != "8")) 
            
            
                puts akt_tah[3].chr.to_s
         
                puts "problem s patznakovym tahom (3) - [" + akt_tah + "]"
                result = "err"
                return result  
            
            else #4
            
               if ((akt_tah[4].chr.to_s != "N") and (akt_tah[4].chr.to_s != "B") and
                    (akt_tah[4].chr.to_s != "R") and (akt_tah[4].chr.to_s != "Q"))  
            
                    puts "problem s patznakovym tahom (4) - [" + akt_tah + "]"
                    result = "err"
                    return result
                    
                else     #5
                        
                    puts "patznakovy tah (5) - [" + akt_tah + "]"
                    result = "ok"
                    return result 
                    
                    
                end #5
            
            end       #4        
       
         end   #3

       end #2
       
   elsif ((akt_tah[0].chr.to_s == "N") or (akt_tah[0].chr.to_s == "B") or
           (akt_tah[0].chr.to_s == "R") or (akt_tah[0].chr.to_s == "Q"))  
                             
           if ((akt_tah[1].chr.to_s >= "1") and (akt_tah[1].chr.to_s <= "8")) 
           
               if (akt_tah[2].chr.to_s != "x") 
       
                   #puts "problem s patznakovym tahom (6) - [" + akt_tah + "]"
                   result = "err"
                   return result      
       
               else  
           
                   if ((akt_tah[3].chr.to_s < "a") or (akt_tah[3].chr.to_s > "h")) 
                   
                      #puts "problem s patznakovym tahom (7) - [" + akt_tah + "]"
                      result = "err"
                      return result      
                   
                   else 
                             
                      if ((akt_tah[4].chr.to_s < "1") or (akt_tah[4].chr.to_s > "8")) 
                   
                          #puts "problem s patznakovym tahom (8) - [" + akt_tah + "]"
                          result = "err"
                          return result  
                      
                      else
                                  
                          #puts "patznakovy tah (9) - [" + akt_tah + "]"
                          result = "ok"
                          return result 
                      
                      end             
                 
                   end   
               
               end    
           
           elsif ((akt_tah[1].chr.to_s >= "a") and (akt_tah[1].chr.to_s <= "h"))
           
              if (akt_tah[2].chr.to_s != "x") 
       
                   #puts "problem s patznakovym tahom (10) - [" + akt_tah + "]"
                   result = "err"
                   return result      
       
               else  
           
                   if ((akt_tah[3].chr.to_s < "a") or (akt_tah[3].chr.to_s > "h")) 
                   
                      #puts "problem s patznakovym tahom (11) - [" + akt_tah + "]"
                      result = "err"
                      return result      
                   
                   else 
                             
                      if ((akt_tah[4].chr.to_s < "1") or (akt_tah[4].chr.to_s > "8")) 
                   
                          #puts "problem s patznakovym tahom (12) - [" + akt_tah + "]"
                          result = "err"
                          return result  
                      
                      else 
                                  
                          #puts "patznakovy tah (13) - [" + akt_tah + "]"
                          result = "ok"
                          return result 
                      
                      end         
                 
                   end   
               
               end    
           
           else
           
                  #puts "problem s patznakovym tahom (14) - [" + akt_tah + "]"
                  result = "err"
                  return result
       
           end
             
   else  

  
       #puts "problem s patznakovym tahom (15) - [" + akt_tah + "]" 
       result = "err"
       return result
  
   end

   #puts "problem s stvorznakovym tahom (16) - [" + akt_tah + "]" 
   result = "err"
   return result

end

# ==================================================================================================
# metoda: <main> = hlavny program
#
# Popis: v hlavnom programe precitame zo vstupu meno suboru, v ktorom je yapis partie, 
#        ktoru mame analyzovat. Cely obsah suboru precitame do retazca, ktorz posleme 
#        do metody "Analzyuj", vratenu hodnotu = vysledok analyzy = zobrazime na vystup
#
# Vstup: nic na prikazovom riadku, vstupom od uzivatela je nazov 
#        textoveho suboru, v ktorom je zapis partie ulozeny
#
# Vystup: program vypise na vystup retazec "ok", ak je zapis sachovej partie
#         v poriadku, ak nie je vypise na vystup retazec "err"
# ==================================================================================================


#puts "zadaj meno suboru: "
filename =  "pgn.txt"
myfile = File.open(filename,"r")
pgnsubor = myfile.read
myfile.close
#puts pgnsubor
vysledok = "err"
vysledok=analyzuj(pgnsubor)
puts "\n" + vysledok
