###DONE
#10/ Pytagorovsky triplet je trojica cisel {a,b,c}, kde a,b,c su prirodzene cisla (cele cisla, vacsie ako 0),
    #pricom plati, ze a*a + b*b = c*c. Teda napr. {3,4,5} je pytagorovsky triplet, kedze plati, ze 
    #3*3 + 4*4 = 5*5.
    #Esituje jediny pytagorovksy triplet {a,b,c}, pre ktory plati, ze a + b + c = 1000. najdite ho a vypiste na vystup.

riesenie = []
for a in 1..1000
  for b in 1..1000
    c = Math.sqrt(a*a + b*b)
      #mame rovnicu
      if a + b + c == 1000
        #mame riesenie
        riesenie.push([a,b,c])
      end
  end
end

#zoradim a vypisem najmensie cislo
puts riesenie.sort[0]

