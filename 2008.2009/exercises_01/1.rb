####DONE
#1/ Na vstupe je textovy retazec. 
#Vypiste na vystup pre kazdu z nasledovnych samohlasok (a,e,y,u,i,o) pocet jej vyskytov v danom vstupnom retazci.

#napr.

#VSTUP: 
#som vstupny retazec

#VYsTUP:
#a - 1
#e - 2
#y - 1
#u - 1 
#i - 0
#o - 1
vstup = gets.chomp
pocetA = pocetE = pocetI = pocetO = pocetU = pocetY = 0
for i in 0...vstup.length
  znak = vstup[i].chr
  if znak == "a"
    pocetA = pocetA + 1
  end
  if znak == "e"
    pocetE = pocetE + 1
  end
  if znak == "i"
    pocetI = pocetI + 1
  end
  if znak == "o"
    pocetO = pocetO + 1
  end
  if znak == "u"
    pocetU = pocetU + 1
  end
  if znak == "y"
    pocetY = pocetY + 1
  end
end
puts "pocet a - " + pocetA.to_s
puts "pocet e - " + pocetE.to_s
puts "pocet i - " + pocetI.to_s
puts "pocet o - " + pocetO.to_s
puts "pocet u - " + pocetU.to_s
puts "pocet y - " + pocetY.to_s
