###myslim ze DONE
#4/ Majme na vstupe tri cisla d, m, y. Vypiste na vstup, ci dane cisla predstavuju korektny datum
   #v tvare d-m-y, kde "d" je den, "m" je mesiac, a "y" je rok, z intervalu od 1-1-1000 do 1-1-3000.
   #Nezabudnite na priestupne roky (priestupny rok je kazde 4 roky, a je dekitelny 4 bez zvysku).
   
#napr. 

#VSTUP: 
#2
#23
#2000

#VYSTUP:
#nekorektny datum
print "den: "
den = gets.chomp
print "mesiac: "
mesiac = gets.chomp
print "rok: "
rok = gets.chomp

if (den.to_i > 31) or (den.to_i < 1) or (mesiac.to_i > 12) or (mesiac.to_i < 1) or (rok.to_i < 1000) or (rok.to_i > 3000)
  puts "NEKOREKTNY DATUM"
else
  #do 7.mesiaca je kazdy neparny 31-dnovy mesiac, okrem februara
  #od 7. mesiaca je kazdy parny 31-dnovy mesiac
  #go!
  if rok.to_i % 4 == 0
    #rok je teda priestupny, a februar ma maximalne 29dni
    if mesiac.to_i == 2
      if den.to_i <= 29
        puts "KOREKTNY DATUM"
      else
        puts "NEKOREKTNY DATUM"
        break
      end
    end
    
    if mesiac.to_i % 2 == 0
      #mesiac je teda parny, ma max. 30dni
        if den.to_i <= 30
          puts "KOREKTNY DATUM"
        else
          puts "NEKOREKTNY DATUM"
          break
        end
      else
        if den.to_i <= 31
          puts "KOREKTNY DATUM"
        else
          puts "NEKOREKTNY DATUM"
          break
        end
      end

    elsif mesiac.to_i >= 8
      if mesiac.to_i % 2 == 0
      #mesiac je teda parny, ma max. 31dni
        if den.to_i <= 31
          puts "KOREKTNY DATUM"
        else
          puts "NEKOREKTNY DATUM"
          break
        end
      else
        if den.to_i <= 30
          puts "KOREKTNY DATUM"
        else
          puts "NEKOREKTNY DATUM"
          break
        end
      end
    
  else
    if mesiac.to_i == 2
      if den.to_i <= 28
        puts "KOREKTNY DATUM"
      else
        puts "NEKOREKTNY DATUM"
        break
      end
    end
    
    if mesiac.to_i <= 7
      if mesiac.to_i % 2 == 0
      #mesiac je teda parny, ma max. 30dni
        if den.to_i <= 30
          puts "KOREKTNY DATUM"
        else
          puts "NEKOREKTNY DATUM"
          break
        end
      else
        if den.to_i <= 31
          puts "KOREKTNY DATUM"
        else
          puts "NEKOREKTNY DATUM"
          break
        end
      end

    elsif mesiac.to_i >= 8
      if mesiac.to_i % 2 == 0
      #mesiac je teda parny, ma max. 31dni
        if den.to_i <= 31
          puts "KOREKTNY DATUM"
        else
          puts "NEKOREKTNY DATUM"
          break
        end
      else
        if den.to_i <= 30
          puts "KOREKTNY DATUM"
        else
          puts "NEKOREKTNY DATUM"
          break
        end
      end
    end
  end
end
