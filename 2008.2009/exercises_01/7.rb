######DONE, ale neotestovane
#7/ Na vstupe su tri realne cisla a, b, c, ktore predstavuju koeficienty kvadratickej rovnice
#v tvare a*(x*x) + b*x + c = 0. Vyrieste kvadraticku rovnicu a vypiste na vystup kolko je x.

#napr.

#VSTUP:
#5,3,-10.8
#VYSTUP:
#-1.8

vstup = gets.chomp
vstup = vstup.split(",")
for x in 0..(vstup.length)-1
  vstup[x] = vstup[x].to_f
end

d = vstup[1] * vstup[1] -4 * vstup[0] * vstup[2]
if d >= 0
  x1 = (-vstup[1] + Math.sqrt(d)) / (2*vstup[0])
    
  x2 = (-vstup[1] - Math.sqrt(d)) / (2*vstup[0])
    
  if x1 == x2
    puts x1
  else
    puts x1
    puts x2
  end
else
  puts "Nema riesenie"
end
