###DONE ale ta kontrola datumov je len taka zeby tam bolo aspon nieco... kontrolu datumov som robil v predchdzajucom cviceni
#5/ Na vstupe je sest cisel - d1,m1,y1, d2, m2, y2 - ktore predstavuju dva datumy - d1-m1-y1 a d2-m2-y2,
   #kde "d" je den, "m" je mesiac, a "y" je rok, z intervalu od 1-1-1000 do 1-1-3000. Vypiste pocet sekund ,
   #ktore predstavuju rozdiel medzi danymi datumami. Ak su datumy zhodne je to 0, inak musi jeden datum
   #predstavovat skorsi termin a druhy neskorsi. Zaujima nas len rozdiel medzi nimi v sekundach, nie ktory
   #datum je skor a ktory neskor.
   
#Napr.

#VSTUP: 31,12,2008,1,1,2009
#VYSTUP: 86400

vstup = gets.chomp
pole = vstup.split(",")
for x in 0..pole.length-1
  pole[x]=pole[x].to_i
end

if pole[0] > 31 or pole[0] < 1 or pole[1] > 12 or pole[1] < 1 or pole[2] < 1000 or pole[2] > 3000 or pole[3] > 31 or pole[3] < 1 or pole[4] > 12 or pole[4] < 1 or pole[5] < 1000 or pole[5] > 3000
  puts "smola"
else
  datumP=[nil,31,29,31,30,31,30,31,31,30,31,30,31]
          #nil, J, F, M, A, M, J, J, A, S, O, N, D
  datumN=[nil,31,28,31,30,31,30,31,31,30,31,30,31]
  
  if pole[2] % 4 == 0
    #priestupny rok
    datumy = datumP.clone
    #puts "1som priestupny"
    pocetPrechodnych = (pole[2] / 4)-1
  else
    #nepriestupny rok
    datumy = datumN.clone
      pocetPrechodnych = pole[2] / 4
  end
  
  prd1 = 0
  for x in datumy[1...pole[1]]
    prd1 = (x * 24 * 60 * 60) + prd1
    #pocet sekund do toho mesiaca
  end

  vysledok1 = prd1 + pole[0] * 24 * 60 * 60 + 24 * 60 * 60 * 365 * (pole[2]-1) + 24 * 60 * 60 * pocetPrechodnych
  
  
  
  if pole[5] % 4 == 0
    #priestupny rok
    datumy = datumP.clone
    pocetPrechodnych = (pole[2] / 4)-1
  else
    #nepriestupny rok
    datumy = datumN.clone
    pocetPrechodnych = pole[2] / 4
  end
  
  prd2 = 0
  for x in datumy[1...pole[4]]
    prd2 = (x * 24 * 60 * 60) + prd2
    #pocet sekund do toho mesiaca
  end

  vysledok2 = prd2 + pole[3] * 24 * 60 * 60 + 24 * 60 * 60 * 365 * (pole[5]-1) + 24 * 60 * 60 * pocetPrechodnych
  puts (vysledok2 - vysledok1).abs
end
