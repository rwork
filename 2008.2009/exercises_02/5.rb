#DONE
#Dvanaste cislo vo Fibonacciho postupnosti (pozri predchadzajuce cvicenie) je prve, 
#ktore obsahuje tri cislice. Zistite a vypiste prve cislo vo Fibonacciho postupnosti, 
#ktore obsahuje 10 cislic.

f = Array.new
x = 1
f[x] = 1
f[x+1] = 1

while f[-1] == f[-1] #nekonecna slucka
  f[x+2] = f[x+1] + f[x]
  if f[x+2].to_s.length >= 10
    puts f[x+2]
    exit
  end
  x = x + 1

end
