#DONE
#Ak vezmeme vsetky prirodzene cisla mensie ako 10, ktore su nasobkami 3 a 5, 
#dostaneme cisla: 3, 5, 6, 9. Ich sucet je 23.
#Najdite a vypiste sucet vsetkych prirodzenych cisiel, ktore su nasobkami 3 a 5,
#a su mensie ako 1000.

vysledok = 0
for x in 1...1000
  if (x % 3 == 0) or (x % 5 == 0)
    vysledok = vysledok + x
  end
end
puts vysledok
