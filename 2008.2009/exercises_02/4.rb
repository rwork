#DONE
#Fibbonaciho postupnost je postupnost cisel, zacinajuca cislami 1 a 1, kde kazde 
#nasledujuce cislo je suctom predoslych dvoch cisel v danej postupnosti.

#Teda plati, ze 

#F(n) = F(n-1) + F(n-2), where F(1) = 1 and F(2) = 1.

#Teda prvych niekolko cisiel vo Fibonacciho postupnosti je:

#F(1) = 1
#F(2) = 1
#F(3) = 2
#F(4) = 3
#F(5) = 5
#F(6) = 8
#F(7) = 13
#F(8) = 21
#F(9) = 34
#F(10) = 55
#F(11) = 89
#F(12) = 144

#Vypocitajte a vypiste sucet vsetkych neparnych cisiel vo Fibonacciho postuponosti,
#ktore nie su vascie ako 4 miliony.
f = Array.new
x = 1
f[x] = 1
f[x+1] = 1

vysledok = 2 #pretoze prve cisla su neparne
while f[-1] <= 4000000
  f[x+2] = f[x+1] + f[x]
  if f[x+2] % 2 != 0
    #je to neparne cislo
    if f[x+2] <= 4000000
      #puts f[x+2]
      vysledok = vysledok + f[x+2]
    end
  end
  x = x + 1

end
puts vysledok
