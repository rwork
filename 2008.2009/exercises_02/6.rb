#DONE!
#1.januara 1900 bol pondelok. September, april, jun a november ma 30 dni. Ostatne mesiace 
#(okrem februara) maju 31 dni. Februar ma 28 dni, len v priestupnych rokoch ma 29 dni.
#Priestupny rok je v roku, ktory je bezo zvysku delitelny 4, no nie ak posledne dve cisla 
#roku su nuly (ibaze by bol taky rok delitelny aj 400).

#Zistite a vypiste kolko nedeli vypadlo v 20. storoci (teda od 01-jan-1901 do 31-dec-2000)
#na prvy den (akehokolvek) mesiaca.

#chcem zistit aky den bol 1. april 1900
#januar + februar + marec
#(((((1 + 31) %7) + 29) %7) + 31) %7 #=> 1, teda pondelok
#1. januara 1901 bol utorok, preto $medzisucet == 2

def analyzujRok(rok)
  if rok % 4 == 0
    return true #je priestupny
  else
    return false #nie je priestupny
  end
end

def analyzujMesiac(mesiace)
  for mesiac in mesiace
    $medzisucet = ($medzisucet + mesiac) % 7
    if $medzisucet == 0 #je to nedela
      #p mesiac
      $pocetNedeli += 1
      #gets
    end
  end
end

$mesiaceP = [31,29,31,30,31,30,31,31,30,31,30,31]
$mesiaceN = [31,28,31,30,31,30,31,31,30,31,30,31]
$medzisucet = 2
$pocetNedeli = 0
for rok in 1901..2000
  #p rok
  #p "====="
  if analyzujRok(rok) #zistim, ci je priestupny, pokracujem ak ano
    analyzujMesiac($mesiaceP)
  else
    analyzujMesiac($mesiaceN)
  end
end

p $pocetNedeli
