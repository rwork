#DONEEEEEE
# "3n+1" problem
# ==============
# 
# Majme nasledovny algoritmus na generovanie postupnosti celych cisel. Zaciname s celym kladnym cislom n. Ak je n parne cislo potom nasledujuce cislo v postupnosti ziskame tak, ze n delime dvoma. Ak je n neparne cislo, tak nasledujuce cislo v postupnosti ziskame tak, ze n nasobime troma a pricitame naviac cislo 1. tento proces opakujeme a tak ziskavame dalsie cisla v postupnosti. Ak nastane situacia ze n = 1, potom dany ciselny rad konci a nepokracujeme vo vytvarani dalsieho cisla.  Napr. ak pociatocne cislo n = 22, potom dostavame nasledovny cisleny rad:
# 
# 22 11 34 17 52 26 13 40 20 10 5 16 8 4 2 1
# 
# predpoklada sa (hoci sa to zatial nepodarilo nikomu dokazat), ze tento ciselny rad skonci s n=1 pre lubovolne pociatocne cele kladne cislo n. Urcite to plati pre lubovolne n < 1 000 000.
# 
# pre dane pociatocne cislo n vieme urcit tzv. cyklicku dlzku danej postupnosti, teda kolko krat musime genrovat dalsie cislo az kym sa dostaneme k n=1. napr. pre vyssie uvedenu postupnost s pociatocnym cislom n=22 je cyklicka dlzka postupnosti 16. 
# 
# Ulohou je pre dane dve cisla i a j urcit maximalnu cyklicku dlzku danej postupnosti pre cisla z intervalu <i,j> vratane krajnych hodnot.
# 
# VSTUP:
# vstupny subor pozostava zo serie dvojic celych cisiel. Na jednom riadku su dve cele cisla, oddelene aspon jednou medzerou. Obidva cisla na riadku su cele vacsie ako 0 a mensie ako 1 000 000. A prve cislo je mensie rovne druhemu. Vstup konci ked sa vo vstupnom subore objavi prazdny riadok.
# 
# VYSTUP:
# Pre kazdu dvojicu celych cisel i,j vypiseme trojicu i,j,k, kde i,j su cisla zo vstupu a k - je maximalne cyklicka dlzka vyssie definovanej postupnosti pre cisla z intervalu <i,j> vratane krajnych hodnot.
# 
# 
# Ukazka vstupu:
# 
# 1 10
# 100 200
# 201 210
# 900 1000
# 
# Zodpovedajuci vystup:
# 
# 1 10 20
# 100 200 125
# 201 210 89
# 900 1000 174

subor1 = File.open("3n_plus_1.pomocne.in", "r")
subor2 = File.open("3n_plus_1.pomocne.out", "w")

for riadok in subor1
    pocet = 1
    pripocitaj = 1
    pocty = []
    riadok = riadok.split #dostanem riadok[i] a riadok[j]
    i = riadok[0].to_i
    i_ = riadok[0].to_i
    j = riadok[1].to_i
    until (pripocitaj+i_) == j
        if i % 2 == 0 #je to parne cislo
            i = i / 2
        else
            i = (i*3)+1
        end
        #gets
        pocet = pocet + 1
        if i == 1
            i = pripocitaj + i_
            pripocitaj += 1
            pocty.push(pocet)
            pocet = 1
        end
    end
    subor2.write(i_.to_s + " " + j.to_s + " " + pocty.sort[-1].to_s + "\n")
end
subor1.close
subor2.close
puts "Automatic shutdown in 5 sec..."
