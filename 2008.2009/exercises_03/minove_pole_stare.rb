def kresli( poradie2, pocetMinOkolo, koniecHry, pole )
    system("clear")
    x = 1
    y = 1
    @@pocetOdkrytychPoli += 1
    for jeden in @@pomocnyZoznam
        if jeden == poradie2
            @@pocetOdkrytychPoli -= 1
            break
        end
    end
    @@pomocnyZoznam.push(poradie2)
    pom = " -"
    cislo = 0
    if koniecHry == true
        i = 0
        POCETMIN.times do
            @@udaje.push([@@suradniceMin[i], " *"])
            i += 1
        end
        @@udaje.uniq!
    else
        @@udaje.push([poradie2, pocetMinOkolo])
        @@udaje.uniq!
    end
    @@udaje.sort!
    if @@pocetOdkrytychPoli == (((SIRKA-2)*(VYSKA-2)) - POCETMIN)
        i = 0
        POCETMIN.times do
            @@udaje.push([@@suradniceMin[i], " *"])
            i += 1
        end
        @@udaje.uniq!
        @@udaje.sort!
        puts "Vyhral si! 8-)"
        koniecHry = true 
    end

    print "  "
    (SIRKA-2).times do
        print cislo = cislo.next
        print " "
    end
    puts
    pism = 65
    (VYSKA-2).times do
        print pism.chr
        pism = pism.succ
        (SIRKA-2).times do
            begin       #toto by sa dalo napisat aj bez vynimiek, ale nic ine mi nenapadlo...
                if x == @@udaje[y][0]
                    print @@udaje[y][1]
                    y += 1
                else
                    print pom
                end
            rescue
                print pom
            end
            x += 1
        end
        print "\n"
    end
    if koniecHry
        gets
        exit
    end
    puts
end

def pocitajporadie(x)
    @@sur[0+x] = @@sur[0+x].chr.downcase.bytes.to_a.to_s.to_i
    @@poradie = SIRKA + 1 + (@@sur[0+x] - 96 - 1)*(SIRKA-2) + (@@sur[0+x]-96-1)*2 + @@sur[1+x].chr.to_i #zo zadaneho napr. A1 som ziskal 1 - @@poradie v hashi
end

def pocitajporadie2
    #@@poradie2 = (@@sur[0+x] -97 )*(SIRKA-2)  + @@sur[1+x].chr.to_i
    @@poradie2 = @@poradie - SIRKA - 1 - ((@@poradie / SIRKA) - 1)*2
end

def zistiPocetMinOkolo(pole)
    @@pocetMinOkolo = 0
    for x in [ pole[@@poradie - SIRKA - 1], pole[@@poradie - SIRKA], pole[@@poradie - SIRKA + 1],
        pole[@@poradie - 1], pole[@@poradie], pole[@@poradie + 1],
        pole[@@poradie + SIRKA - 1], pole[@@poradie + SIRKA], pole[@@poradie + SIRKA + 1] ] 
        if x == true
            @@pocetMinOkolo += 1  
        end
    end
end

VYSKA = 6 #pri hre je vyska a sirka o 2 dva mensie
SIRKA = 6 #minimum je 6
POCETMIN = 3
@@pocetOdkrytychPoli = 0
@@udaje = [] #na ukladanie miest, ktore som uz zadal a sa maju zobrazit na mape
@@pomocnyZoznam = [0]
@@suradniceMin = []

pole = Hash.new
puts "Tvar suradnic: napr. \"A1\""
puts "Pre oznacenie miny: napr. \"?A1\""
puts "Pokracovat..."
gets
#naplnim hash cislami, ziadne neobsahuje minu (false)
for x in 1..(VYSKA)*(SIRKA)
    pole.merge!(x=>false)
end
#teraz to trosku pomixujem...
p = []
POCETMIN.times do
    min = SIRKA+2
    max = (SIRKA*VYSKA)-SIRKA-1
    ran = (min + rand(max-min)).to_i
    while ran % SIRKA == 0 or (ran-1) % SIRKA == 0 or p.grep(ran) != []
        ran = (min + rand(max-min)).to_i
    end
    pole.merge!({ran => true}) #hodim minu do hry
    @@suradniceMin.push(ran - SIRKA - 1 - ((ran / SIRKA) - 1)*2) #vyrobim suradnicu2 miny(ako metoda pocitajporadie2 )
    p.push ran
end
kresli(0," -", false, pole) #vykreslim prvotnu mapu
while 1 == 1
    puts "\nZadaj suradnice: "
    @@sur = gets.chomp
    if !(@@sur =~ /[a-#{(VYSKA-2+96).chr}A-#{(VYSKA-2+64).chr}][1-#{SIRKA-2}]/)
        if !(@@sur =~ /\?[a-#{(VYSKA-2+96).chr}A-#{(VYSKA-2+64).chr}][1-#{SIRKA-2}]/)
            # (@@sur[0].chr == "?" and (@@sur.length >= 4 or @@sur.length <= 2 or (@@sur[1].chr.to_i) > (VYSKA-2+96) or @@sur[2].chr.to_i > (SIRKA-2) or @@sur[2].chr.to_i == 0 or @@sur[1].chr.to_i != 0)) or
            # (@@sur[0].chr != "?" and (@@sur.length >= 3 or @@sur.length <= 1 or (@@sur[0].chr.to_i) > (VYSKA-2+96) or @@sur[1].chr.to_i > (SIRKA-2) or @@sur[1].chr.to_i == 0 or @@sur[0].chr.to_i != 0))
            puts "Zadal si nespravne hodnoty"
            gets
        end
    else
        if @@sur[0].chr == "?"
            pocitajporadie(1)
            pocitajporadie2
            kresli( @@poradie2, " ?", false, pole)
        else
            pocitajporadie(0)
            pocitajporadie2
            #zacina sa kontrola okolitych policok, ak sa najde mina v okoli, tak si ulozim poziciu policka 
            #ak sa najde mina, prehral som
            if pole[@@poradie] == true #nasiel som minu
                POCETMIN.times do
                    @@poradie = pole.index(true)
                    pocitajporadie2
                    kresli( @@poradie2, " *", true, pole)
                    pole.merge!({@@poradie => false})
                end
            end

            #zistujem pocet min okolo vybraneho policka
            zistiPocetMinOkolo(pole)
            #p @@sur
            #p @@poradie
            #p @@poradie2
            #gets
            kresli( @@poradie2, " "+@@pocetMinOkolo.to_s, false, pole )
        end
    end
end
