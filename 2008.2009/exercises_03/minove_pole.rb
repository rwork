def kresli( udaje, otaznik, koniecHry, poradie )
    system("clear")
    if otaznik == false
        @@pocetOdkrytychPoli += 1
        for jeden in @@pomocnyZoznam
            if jeden == poradie
                @@pocetOdkrytychPoli -= 1
                break
            end
        end
        @@pomocnyZoznam.push(poradie)
    end
    pismeno = "@"
    VYSKA.times do |y|
        if y == (VYSKA-2) then break end
        if y != 0 or y < (VYSKA)
            print pismeno = pismeno.next
            SIRKA.times do |x|
                x = x + 1
                if (y != (VYSKA-1)) and (x < ((SIRKA*VYSKA)-SIRKA-1)) and (x % SIRKA != 0) and ((x-1) % SIRKA != 0)
                    #if (x > (SIRKA+1)) and (x < ((SIRKA*VYSKA)-SIRKA-1)) and (x % SIRKA != 0) and ((x-1) % SIRKA != 0)
                    #p "b"
                    print udaje[((SIRKA*(y+1))+(x-1))]
                end
                #else
                #print "a"
                #end
            end
            print "\n"
        end
    end
    if @@pocetOdkrytychPoli == (((VYSKA-2)*(SIRKA-2))-POCETMIN) or koniecHry
        puts "Koniec hry"
        gets
        exit
    end
end

def pocitajporadie(x)
    #puts @@sur[0+x] = @@sur[0+x].chr.downcase.bytes.to_a.to_s.to_i
    #puts @@sur[0+x] = @@sur[0+x]
    #gets
    poradie = SIRKA + 1 + (@@sur[0+x] - 96 - 1)*(SIRKA-2) + (@@sur[0+x]-96-1)*2 + @@sur[1+x].chr.to_i #zo zadaneho napr. A1 som ziskal 1 - poradie v hashi
    return poradie
end


def zistiPocetMinOkolo(pole, poradie)
    @@pocetMinOkolo = 0
    for x in [ pole[poradie - SIRKA - 1], pole[poradie - SIRKA], pole[poradie - SIRKA + 1],
        pole[poradie - 1], pole[poradie], pole[poradie + 1],
        pole[poradie + SIRKA - 1], pole[poradie + SIRKA], pole[poradie + SIRKA + 1] ] 
        if x == true
            @@pocetMinOkolo += 1  
        end
    end
end

VYSKA = 6 #pri hre je vyska a sirka o 2 dva mensie
SIRKA = 6 #minimum je 6
POCETMIN = 4
@@pocetOdkrytychPoli = -1
udaje = [" -"]*(VYSKA*SIRKA) #na ukladanie miest, ktore sa maju zobrazit na mape
@@pomocnyZoznam = [0]
@@suradniceMin = []

pole = Hash.new
puts "Tvar suradnic: napr. \"A1\""
puts "Pre oznacenie miny: napr. \"?A1\""
puts "Pokracovat..."
gets
#naplnim hash cislami, ziadne neobsahuje minu (false)
for x in 1..(VYSKA)*(SIRKA)
    pole.merge!(x=>false)
end
#teraz to trosku pomixujem...
p = []
POCETMIN.times do
    min = SIRKA+2
    max = (SIRKA*VYSKA)-SIRKA-1
    ran = (min + rand(max-min)).to_i
    while ran % SIRKA == 0 or (ran-1) % SIRKA == 0 or p.grep(ran) != []
        ran = (min + rand(max-min)).to_i
    end
    pole.merge!({ran => true}) #hodim minu do hry
    @@suradniceMin.push(ran - SIRKA - 1 - ((ran / SIRKA) - 1)*2) #vyrobim suradnicu2 miny(ako metoda pocitajporadie2 )
    p.push ran
end
kresli(udaje, false, false, "") #vykreslim prvotnu mapu
while 1 == 1
    puts "\nZadaj suradnice: "
    @@sur = gets.chomp
    if !(@@sur =~ /[a-#{(VYSKA-2+96).chr}A-#{(VYSKA-2+64).chr}][1-#{SIRKA-2}]/)
        if !(@@sur =~ /\?[a-#{(VYSKA-2+96).chr}A-#{(VYSKA-2+64).chr}][1-#{SIRKA-2}]/)
            # (@@sur[0].chr == "?" and (@@sur.length >= 4 or @@sur.length <= 2 or (@@sur[1].chr.to_i) > (VYSKA-2+96) or @@sur[2].chr.to_i > (SIRKA-2) or @@sur[2].chr.to_i == 0 or @@sur[1].chr.to_i != 0)) or
            # (@@sur[0].chr != "?" and (@@sur.length >= 3 or @@sur.length <= 1 or (@@sur[0].chr.to_i) > (VYSKA-2+96) or @@sur[1].chr.to_i > (SIRKA-2) or @@sur[1].chr.to_i == 0 or @@sur[0].chr.to_i != 0))
            puts "Zadal si nespravne hodnoty"
            gets
        end
    else
        if @@sur[0].chr == "?"
            poradie = pocitajporadie(1)
            udaje[poradie-1] = " ?"
            #pocitajporadie2
            kresli(udaje, true, false, poradie)
        else
            poradie = pocitajporadie(0) #A1 = 8
            #p pocitajporadie2
            #zacina sa kontrola okolitych policok, ak sa najde mina v okoli, tak si ulozim poziciu policka 
            #ak sa najde mina, prehral som
            if pole[poradie] == true #nasiel som minu
                POCETMIN.times do
                    poradie = pole.index(true)
                    #pocitajporadie
                    udaje[poradie-1] = " *"
                    pole.merge!({poradie => false})
                end
                kresli( udaje, false, true, poradie)
            else

            #zistujem pocet min okolo vybraneho policka
            zistiPocetMinOkolo(pole, poradie)
            udaje[poradie-1] = " "+@@pocetMinOkolo.to_s
            kresli(udaje, false, false, poradie )
            end
        end
    end
end
