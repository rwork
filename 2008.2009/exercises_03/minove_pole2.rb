# Miner
# 
# Hrali ste uz hru miner (Minesweper). Tato jednoducha hra je sucastou jedneho nemenovaneho operacneho systemu.
# Cielom v nej je najst vsetky miny, ktore su skryte na polickach v poli velkosti MxN.
# 
# na zaciatku su vsetky policka skryte. Ak nejake policko odkryjeme, potom je tam bude mina alebo cele kladne cislo
# hovoriace s kolkymi minami susedi (po diagonalach, vertikalach i horizontalach) dane policko.
# 
# Majme minove pole velkosti 4x4. Nech je mina oznacena znakom "*". A nech vieme kde su umiestnene miny, napr.
# 
# *... 
# .... 
# .*.. 
# ....
# 
# Nasou ulohou je vypisat dane pole, nielen s poziciou min, ale aj so zodpovedajucimi cislami, teda k predoslemu vstupu 
# vypisat nasledovny vystup:
# 
# *100
# 2210
# 1*10
# 1110
# 
# VSTUP:
# 
# Na vstupe su definocie jedneho alebo viacerych minovych poli. Najprv je dvojica celych kladnych cisel m,n pricom m,n < 10. Tieto dve cisla udavaju velkost minoveho pola.
# potom je tu po riadkoch dane vlastne minove pole. Pozicie min su oznacene znakom "*". Ostatne pozicie su oznacene znakom ".". Vstup konci ak narazime na prazdny riadok.
# 
# VYSTUP:
# 
# Mame na vystup vypisat tie iste minove polia no aj so zodpovedajucimi ciselnymi hodnotami namiesto znakov ".".
# 
# Ukazka vstupu:
# 
# 4 4   
# *...  
# .... 
# .*..  
# ....  
# 3 5
# **... 
# ..... 
#       
# .*...
# 
# Zodpovedajuci vystup:
# 
# *100
# 2210
# 1*10
# 1110
# 
# **100
# 33200
# 1*100
#
def kresli
    system("clear")
    x = 1
    y = 0
    pom = " -"
    cislo = 0

    print "  "
    (SIRKA-2).times do
        print cislo = cislo.next
        print " "
    end
    puts
    pism = 65
    (VYSKA-2).times do
        print pism.chr
        pism = pism.succ
        (SIRKA-2).times do
            print " " + @@udaje[y][1].to_s
            y += 1
        end
        print "\n"
    end
end

def pocitajporadie2 #z cisla 8 urobi poradie, ktore vidim na mape, teda 1
    @@poradie2 = @@poradie - SIRKA - 1 - ((@@poradie / SIRKA) - 1)*2
end

def zistiPocetMinOkolo(pole)
    @@pocetMinOkolo = 0
    for x in [ pole[@@poradie - SIRKA - 1], pole[@@poradie - SIRKA], pole[@@poradie - SIRKA + 1],
        pole[@@poradie - 1], pole[@@poradie], pole[@@poradie + 1],
        pole[@@poradie + SIRKA - 1], pole[@@poradie + SIRKA], pole[@@poradie + SIRKA + 1] ] 
        if x == true
            @@pocetMinOkolo += 1  
        end
    end
end

@@udaje = [] #na ukladanie miest cisiel a k nim prisluchajucim poctom min
@@suradniceMin = [] #ukladam si suradnice min
pole = Hash.new
POCETMIN = 3

puts "zadaj vysku:"
begin
    vyska = gets.chomp
    vyska = vyska.to_i
end while vyska > 10
puts "vyska: #{vyska}"
puts "zadaj sirku: "
begin
    sirka = gets.chomp
    sirka = sirka.to_i
end while sirka > 10
puts "sirka: #{sirka}"

VYSKA = vyska + 2  #pri vypoctoch sa to hodi... vlastne z pola 4x4 urobim 6x6 kvoli kontrole okolitych policok na pocet min
SIRKA = sirka + 2

#naplnim hash cislami, ziadne neobsahuje minu (false)
for x in 1..(VYSKA)*(SIRKA)
    pole.merge!(x=>false)
end
#teraz to trosku pomixujem...
p = [] #pomocna premenna
POCETMIN.times do
    min = SIRKA+2
    max = (SIRKA*VYSKA)-SIRKA-1
    ran = (min + rand(max-min)).to_i
    while ran % SIRKA == 0 or (ran-1) % SIRKA == 0 or p.grep(ran) != [] #kontrolujem vygenerovanie suradnice, ci su zobrazitelne na mape
        ran = (min + rand(max-min)).to_i #ak ano, tak je vsetko v poriadku
    end
    pole.merge!({ran => true}) #hodim minu do hry
    @@suradniceMin.push(ran - SIRKA - 1 - ((ran / SIRKA) - 1)*2) #vyrobim suradnicu2 miny(ako metoda pocitajporadie2 )
    p.push ran #pre buducu kontrolu, ci uz som nahodou nevygeneroval to iste cislo
end
for krat in (SIRKA+2)..((pole.length)-SIRKA-1)
    if krat % SIRKA == 0 or (krat-1) % SIRKA == 0 #aby som nepracoval s "krat", ktory je mimo viditelneho pola
    else
        @@poradie = krat
        pocitajporadie2 #vypocita @@poradie2
        @@poradie2
        if pole[@@poradie] == true
            @@pocetMinOkolo = "*"
        else
            zistiPocetMinOkolo(pole) #vrati @@pocetMinOkolo
        end
        @@udaje.push([@@poradie2, @@pocetMinOkolo])
    end
end
kresli
