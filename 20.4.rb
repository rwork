#otvorime subor len na citanie a vypiseme obsah
#mojsubor = File.open("text.txt", "r")
#obsahsuboru=mojsubor.read
#puts obsahsuboru
#mojsubor.close

#while riadok = mojsubor.gets
#  puts riadok
#end
#mojsubor.close

#File.open("text.txt","r") do |sub|
#  obsah = sub.read
#  puts "Pocet znakov: "+obsah.length.to_s
#end

#otvorime subor na zapis
#mojsubor = File.open("1.rb", "w")
#  mojsubor.puts "Prvy riadok"
#  mojsubor.puts "Druhy riadok"
#mojsubor.close

#mojsubor = File.open("text.txt", "w")
#  ret = "Prvy riadok\n"
#  ret = ret + "Druhy riadok\n"
#  mojsubor.puts ret
#mojsubor.close

=begin
i=0.to_i
subor1 = File.open("zdroj","rb")
subor2 = File.open("kopia.txt","wb")
obsahsuboru=subor1.read
pocet=obsahsuboru.length.to_i
subor1.close

subor1 = File.open("zdroj","rb")
while (i<pocet-1)
  znak = obsahsuboru.getc
  subor2.print znak.chr
  i+1
end
subor1.close
subor2.close
=end

=begin
subor1 = File.open("zdroj","rb")
subor2 = File.open("kopia.txt","wb")
while znak = subor1.getc
  subor2.putc znak
end
subor1.close
subor2.close

REGULARNE VYRAZY
/Ruby/ --> Ruby
/[Rr]uby/ --> Ruby, ruby
/^Ruby/ --> Ruby*
/$Ruby/ --> *Ruby
/\w+1/ -->konciace 1
/\d+a/ -->
/w -->vsecko (cislica, znak, podciarkovnik)
/d -->cislica
=end

text = "First/class thinking!"
print "Scan metoda: "
puts text.scan(/\w+/)
print "Split metoda: "
puts text.split("/")