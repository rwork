def analyzujriadokhlavicky(riadok)

#puts "sme v arh"

pos = 0
dlzkar = riadok.length

puts "analyzujeme -> {" + riadok + "} "
puts "dlzkar -> {" + dlzkar.to_s + "} "

result = "ok"

#puts "riadok[0] -> {" + riadok[0].to_s + "} "
#puts "riadok[dlzkar-1] -> {" + riadok[dlzkar-1].to_s + "} "

if (riadok[0].chr.to_s != "[") or (riadok[dlzkar-1].chr.to_s != "]")
  puts "CHYBA1!"
  return "err"
end

pos = 1
while ((riadok[pos].chr.to_s ==" ") or (riadok[pos].chr.to_s == "\t"))
  pos = pos + 1
   if (pos > dlzkar-2)
     puts "CHYBA2!"
     return "ERR"
   end
end

puts "pos -> {" + pos.to_s + "} "

tagname = ""
while ((riadok[pos].chr.to_s !=" ") and (riadok[pos].chr.to_s != "\t"))

  puts "riadok[pos] -> " + riadok[pos].to_s + "(" + riadok[pos].chr + ")"
  puts "pos -> {" + pos.to_s + "}, tagname -> {" + tagname + "}"
  
  tagname = tagname + riadok[pos].chr.to_s
  pos = pos + 1
   if (pos > dlzkar-2)
     puts "CHYBA3!"
     return "ERR"
   end
end

puts "tagname -> {" + tagname + "} "

return "ok"
end

def analyzujhlavicku(hlavicka)
  
  result = "ok"
  hlavicka.each_line do |riadok|
    result = analyzujriadokhlavicky(riadok.chomp)
      if (result == "ERR")
        return result
      end
  end
  
  return result
  end

def analyzujtahy(tahy)

  return "ok"
  
end

def analyzuj(pgnsubor)

  result = "ok"

  hlavicka = ""
  tahy = ""

  citamehlavicku = 0
  citametahy = 0

  pgnsubor.each_line do |riadok|

    if (riadok.strip.length !=0)

      if (citamehlavicku == 0)
          citamehlavicku = 1
          hlavicka = hlavicka + riadok
      
      elsif (citamehlavicku == 1)
      
         hlavicka = hlavicka + riadok
      
      elsif ((citamehlavicku==2) and (citametahy==0))
      
        citametahy=1
        tahy = tahy + riadok
      
      elsif ((citamehlavicku==2) and (citametahy==1))
      
        tahy = tahy + riadok
      
      end
    
    else
    
      if (citamehlavicku==1)   
        citamehlavicku=2    
      elsif (citametahy==1)    
        citametahy=2
      end
    
    end
  
  end  
  
  #puts "HLAVICKA:"
  #puts hlavicka
 
  #puts "TAHY:"
  #puts tahy
  
  result = analyzujhlavicku(hlavicka)
  if (result !="ok")
      return "err"
  end
  
  if (result == "ok")
    result = analyzujtahy(tahy)
  end

  
  
  
  
  return result
  
end

puts "zadaj meno suboru: PGN.TXT"
filename = "pgn.txt"
myfile = File.open(filename,"r")
pgnsubor = myfile.read
myfile.close
#puts pgnsubor
vysledok = "ERR"
vysledok=analyzuj(pgnsubor)
puts vysledok